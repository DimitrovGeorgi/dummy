package com.example.dummy.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransferController {

    @PostMapping("/withdraw")
    public ResponseEntity<String> withdraw() {

        boolean isSuccess = Math.random() < 0.5;

        if (isSuccess) {
            return new ResponseEntity<>("Withdrawal successful from Dummy API", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Withdrawal failed from Dummy API", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/topUp")
    public ResponseEntity<String> topUp() {

        boolean isSuccess = Math.random() < 0.5;

        if (isSuccess) {
            return new ResponseEntity<>("TopUp successful from Dummy API", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("TopUp failed from Dummy API", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // just test purpose 



}
